#ifndef MANDELBROT_H
#define MANDELBROT_H
#include "types.h"

extern std::map<ipoint, double, ipointCmp> oldValues; //cache

double getIterations (point p, num maxIter, bool arbPrecision);
double arbGetIterations (point p, num maxIter);
double fastGetIterations (dpoint p, num maxIter); 
#endif
