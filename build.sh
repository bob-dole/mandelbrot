NAME=mandelbrot
OPTIONS=-"std=c++11 -pedantic -Werror -Wall -g -o $NAME"
LINK="-lSDL -lGL -lGLU -lgmpxx -lgmp"
g++ $OPTIONS *.cpp $LINK
