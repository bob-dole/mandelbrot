#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include <iostream>
#include "types.h"
#include "mandelbrot.h"

//GUI stuff
unsigned int w;             //screen width in pixels
unsigned int h;             //screen height in pixels
bool running = true;
num leftEdge = -3;          //Rendered rect within the set
num bottomEdge = -1.5;
num width = 4;
num height = 3;
num maxIter = 25;
double SPEED = 0.1;         //Factor to move by
double ZSPEED = 0.5;        //Factor to zoom by
ipoint start = {-1,-1};     //Start point of a click-to-zoom
bool forceDeep = false;
int shades = 25;            //colouring smoothness

//prototypes
bool resizeWindow (int _w, int _h);
void render (bool useOldValues = false);
void keystroke (SDLKey k);
void click (SDL_MouseButtonEvent e);
color getColorForNIterations (double iter);
point mapPixelCoordToMandelbrotCoord (int sx, int sy);

bool initSDL () {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        return false;
    return resizeWindow(640, 480);
}

bool initGL() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0,0,w,h);

    glClearColor(0, 0, 0, 1); 
    
    GLenum error = glGetError();
    return error == GL_NO_ERROR;
}

bool resizeWindow (int _w, int _h) {
    w = _w;
    h = _h;
    if (!initGL())
        return false;
    if (SDL_SetVideoMode(w, h, 32, SDL_OPENGL | SDL_RESIZABLE) == NULL)
        return false;
    SDL_WM_SetCaption("mandelbrot", NULL);
    render();
    return true;
}

int main (int argc, char** argv) {
    if (!(initSDL() && initGL())) return 1;             //Init, on fail then return 1
    
    SDL_Event event;
    while (running) {                                   //until the user quits
        while (SDL_PollEvent(&event)) {                 //handle all buffered events
            switch (event.type) {
                case SDL_QUIT:                          //handle quitting
                    running = false;
                    break;

                case SDL_KEYDOWN:                       //handle keystrokes
                    keystroke(event.key.keysym.sym);
                    break;

                case SDL_MOUSEBUTTONUP:
                    click(event.button);
                    break;

                case SDL_VIDEORESIZE:                   //handle resizing
                    resizeWindow(event.resize.w, event.resize.h);
                    break;
            }
        }
    }
    
    return 0;
}

void render (bool useOldValues) {
    if (!useOldValues) {
        oldValues.clear();
        std::cout << "lowerleft: (" << leftEdge << ", " << bottomEdge << ") " <<
            "width: " << width << " height: " << height << " maxIter: " << maxIter << std::endl;
    } else {
        std::cout << "redrawing" << std::endl;
    }
    start = {-1, -1}; //any zoom started is cancelled
 
    bool arbitraryPrecision = false;
    num boundary = num("2e-12");
    arbitraryPrecision = (width < boundary) || forceDeep;

    if (arbitraryPrecision)
        std::cout << "deep zoom mode" << std::endl;

    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_POINTS);

    for (float x = 0; x < w; x++) {
        for (float y = 0; y < h; y++) {
            ipoint key = {(int)x,(int)y};
            point mcoord = mapPixelCoordToMandelbrotCoord (x, y);
            double iter;
            if (useOldValues)
                iter = oldValues[key];
            else
                iter = getIterations(mcoord, maxIter, arbitraryPrecision);
            oldValues[key] = iter; //cache solution
            color c = getColorForNIterations(iter);
            glColor3f(c.r, c.g, c.b);
            glVertex2f(((x/w)*2)-1, (((h-y)/h)*2)-1);
        }

        if (!useOldValues && ((int)x % 10) == 0) {
            std::cout << "\r" << x << "/" << w;
            fflush(stdout);
        }
    }

    glEnd();
    SDL_GL_SwapBuffers();
    std::cout << "\r[READY]\r";
    fflush(stdout);
}

void rerender () {
    render(true);
}

void setMaxIter () {
    num recip(0);
    mpf_div(recip.get_mpf_t(), mpf_class(1).get_mpf_t(), width.get_mpf_t());
    double timesViewHalved = log2(recip.get_ui());

    maxIter = 25;
    for (int i = 0; i < timesViewHalved; i++)
        maxIter *= 1.3;

    if(maxIter < 25) maxIter = 25;
}

/// COLOURING //////////////////////////////////////////////////////////////////
color getSingleColor (double iter) {
    if (iter == -1) return colorc(0,0,0);

    float shadeOfGray = (int)fmod(iter, shades); //map into a cycle of 0 to shades-1
    shadeOfGray /= shades; //map to 0-1

    return colorc(shadeOfGray, 0, 0);
}

color getGradient (double iter) {
    int shades2 = shades*2;
    if (iter == -1) return colorc(0,0,0);

    float shade = (int)fmod(iter,shades2); //map into a cycle of 0 to shades2-1

    if (shade < (shades2/2)-(shades2/40))
        return colorc(shade/shades2, 0, 0);
    else if (shade > ((shades2/2)+(shades2/40)))
        return colorc(0,0,1-(shade/shades2));
    else
        return colorc(shade/shades2, 0, 1-(shade/shades2));
}

//-1 means the number of iterations is either too high or the pt. doesn't escape
color getColorForNIterations (double iter) {
    //return getSingleColor(iter);
    return getGradient(iter);
}
////////////////////////////////////////////////////////////////////////////////

point mapPixelCoordToMandelbrotCoord (int sx, int sy) {
    point p = {leftEdge+(((double)sx/w)*width), bottomEdge+(((double)sy/h)*height)};
    return p;
}

//////// GUI CONTROLS //////////////////////////////////////////////////////////
void move_up    () { bottomEdge -= SPEED*height; render(); }
void move_down  () { bottomEdge += SPEED*height; render(); }
void move_left  () { leftEdge   -= SPEED*width; render(); }
void move_right () { leftEdge   += SPEED*width; render(); }

void zoom_in () {
    num _width = width;
    num _height = height;

    _width -= ZSPEED*width;
    _height -= ZSPEED*height;
    leftEdge += (ZSPEED*width)/2;
    bottomEdge += (ZSPEED*height)/2;
    width = _width;
    height = _height;

    setMaxIter();
    render();
}

void zoom_out () {
    num _width = width;
    num _height = height;

    _width += ZSPEED*width;
    _height += ZSPEED*height;
    leftEdge -= (ZSPEED*width)/2;
    bottomEdge -= (ZSPEED*height)/2;
    width = _width;
    height = _height;

    setMaxIter();
    render();
}

void doMouseZoom (int endX, int endY) {
    if (!(start.x == endX && start.y == endY)) {
        num oldWidth = width;
        num oldHeight = height;

        int leftmost = start.x < endX ? start.x : endX;
        int rightmost = start.x > endX ? start.x : endX;
        int topmost = start.y > endY ? start.y : endY;
        int bottommost = start.y < endY ? start.y : endY;
        ipoint upperLeft = {leftmost, topmost};
        ipoint lowerRight = {rightmost, bottommost};

        point num_upperLeft = mapPixelCoordToMandelbrotCoord(upperLeft.x, upperLeft.y);
        point num_lowerRight = mapPixelCoordToMandelbrotCoord(lowerRight.x, lowerRight.y);

        leftEdge = num_upperLeft.x;
        bottomEdge = num_lowerRight.y;
        width = num_lowerRight.x - num_upperLeft.x;
        height = num_upperLeft.y - num_lowerRight.y;

        setMaxIter();
        render();
    }

    start = {-1,-1};
}

void keystroke (SDLKey k) {
    if (k == SDLK_ESCAPE)
        running = false;
    else if (k == SDLK_UP)
        move_up();
    else if (k == SDLK_DOWN)
        move_down();
    else if (k == SDLK_LEFT)
        move_left();
    else if (k == SDLK_RIGHT)
        move_right();
    else if (k == SDLK_RETURN)      //Zoom in on return
        zoom_in();
    else if (k == SDLK_BACKSPACE)   //Zoom out on backspace
        zoom_out();
    else if (k == SDLK_MINUS) {     //Decrease iterations on -
        maxIter -= maxIter*0.1;
        render();
    } else if (k == SDLK_EQUALS) {  //Increase iterations on =
        maxIter += maxIter*0.1;
        render();
    } else if (k == SDLK_r) {       //Reset zoom on R
        leftEdge = -3;
        bottomEdge = -1.5;
        width = 4;
        height = 3;
        setMaxIter();
        render();
    } else if (k == SDLK_s) {       //Force buffer swap
        SDL_GL_SwapBuffers();
        SDL_GL_SwapBuffers();
    } else if (k == SDLK_j) {       //Half shades on j
        shades /= 2;
        if (shades <= 1) shades = 2;
        rerender();
    } else if (k == SDLK_k) {       //Double shades on k
        shades *= 2;
        rerender();
    } else if (k == SDLK_d) {       //Toggle arbitrary precision on d
        forceDeep = !forceDeep;
    } else if (k == SDLK_q) {       //Recalculate on q
        render();
    }
}

void click (SDL_MouseButtonEvent e) {
    if (e.button == SDL_BUTTON_LEFT) {
        if (start.x == -1) {
            start = {e.x, e.y};
        } else {
            doMouseZoom(e.x, e.y);
        }
    } else {
        start = {-1, -1};
    }
}
////////////////////////////////////////////////////////////////////////////////

