#include "mandelbrot.h"

std::map<ipoint, double, ipointCmp> oldValues = std::map<ipoint, double, ipointCmp>();

//Returns -1 is the correct answer is >= maxIter
double getIterations (point p, num maxIter, bool arbPrecision) {
    if (arbPrecision) {
        return arbGetIterations(p, maxIter);
    } else {
        dpoint dp;
        dp.x = p.x.get_d();
        dp.y = p.y.get_d();
        return fastGetIterations(dp, maxIter);
    }
}

double arbGetIterations (point p, num maxIter) {
    double iter = 0;

    point z = {0,0};
    while (iter < maxIter) {
        if (((z.x*z.x)+(z.y*z.y)) > 4)
            return iter;
        
        point _z = z;
        _z.x = ((z.x*z.x) - (z.y*z.y)) + p.x;
        _z.y = (2*z.x*z.y) + p.y;
        z = _z;

        iter++;
    }

    return -1;

} 
double fastGetIterations (dpoint p, num maxIter) {
    double iter = 0;

    dpoint z = {0,0};
    while (iter < maxIter) {
        if (((z.x*z.x)+(z.y*z.y)) > 4)
            return iter;
        
        dpoint _z = z;
        _z.x = ((z.x*z.x) - (z.y*z.y)) + p.x;
        _z.y = (2*z.x*z.y) + p.y;
        z = _z;

        iter++;
    }

    return -1;

}
