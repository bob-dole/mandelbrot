#ifndef TYPES_H
#define TYPES_H
#include <math.h>
#include <gmpxx.h>
#include <map>

typedef mpf_class num;
typedef unsigned int uint;

typedef struct color {
    float r, g, b;
} color;

typedef struct point {
    num x, y;
} point;

typedef struct dpoint {
    double x, y;
} dpoint;

typedef struct ipoint {
    int x, y;
} ipoint;

struct ipointCmp {
      bool operator() (const ipoint& lhs, const ipoint& rhs) const {
          return lhs.x<rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
      }
};

color colorc (float r, float g, float b);

#endif
